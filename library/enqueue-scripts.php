<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_scripts' ) ) :
	function foundationpress_scripts() {

	// Enqueue the main Stylesheet.
	wp_enqueue_style( 'main-stylesheet', get_template_directory_uri() . '/assets/stylesheets/foundation.css', array(), '2.6.1', 'all' );

	// Deregister the jquery version bundled with WordPress.
	wp_deregister_script( 'jquery' );

	// CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
	wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js', array(), '2.1.0', false );

	// If you'd like to cherry-pick the foundation components you need in your project, head over to gulpfile.js and see lines 35-54.
	// It's a good idea to do this, performance-wise. No need to load everything if you're just going to use the grid anyway, you know :)
	wp_enqueue_script( 'foundation', get_template_directory_uri() . '/assets/javascript/foundation.js', array('jquery'), '2.6.1', true );

	// Add the comment-reply library on pages where it is necessary
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	}

	add_action( 'wp_enqueue_scripts', 'foundationpress_scripts' );
endif;

//hide Toolset fields from all pages
add_action('admin_enqueue_scripts', 'toolset_admin_styles');
function toolset_admin_styles() {

    if(get_post_type() == 'page' || get_post_type() == 'post') {
        echo '<style type="text/css">
          	#types-information-table {display:none;}

        	#wpcf-post-relationship .handlediv {display:none;}

        	#wpcf-post-relationship h2.hndle ui-sortable-handle {display:none;}
        </style>';
    }
}

//hide Home Slider fields from all pages except 'Home'
add_action('admin_enqueue_scripts', 'home_slider_admin_styles');
function home_slider_admin_styles() {
    $arr = array(21);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-home-slider {display:none;}
        </style>';
    }
}

//hide Staff fields from all pages except 'Staff'
add_action('admin_enqueue_scripts', 'staff_admin_styles');
function staff_admin_styles() {
    $arr = array(90);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-staff-member {display:none;}
        </style>';
    }
}

//hide Budget and Audits fields from all pages except 'Committees'
add_action('admin_enqueue_scripts', 'committees_admin_styles');
function committees_admin_styles() {
    $arr = array(82);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-budget {display:none;}
          #types-child-table-page-audit {display:none;} 
        </style>';
    }
}

//hide Minutes fields from all pages except 'Board of Directors'
add_action('admin_enqueue_scripts', 'minutes_admin_styles');
function minutes_admin_styles() {
    $arr = array(59);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-minutes {display:none;}
        </style>';
    }
}

//hide MPO fields from all pages except 'MPO'
add_action('admin_enqueue_scripts', 'mpo_admin_styles');
function mpo_admin_styles() {
    $arr = array(336);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
            #types-child-table-page-policy-minutes {display:none;}
            #types-child-table-page-advisory-minutes {display:none;}
            #types-child-table-page-trans-minutes {display:none;}
            #types-child-table-page-citizens-minutes {display:none;}
        </style>';
    }
}

//hide Glossary Term fields from all pages except 'Glossary'
add_action('admin_enqueue_scripts', 'glossary_admin_styles');
function glossary_admin_styles() {
    $arr = array(41);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-glossary-term {display:none;}
        </style>';
    }

    if (in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #postdivrich {display:none;}
        </style>';
    }
}