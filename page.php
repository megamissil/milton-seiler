<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="page" role="main">

    <?php while ( have_posts() ) : the_post(); ?>
        
        <div class="row subpage-title">
            <div class="small-12 columns">
                <h1><?php the_title(); ?></h1>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="small-12 medium-11 large-10 medium-centered columns">
                <div class="subpage-content">
                    <?php get_template_part( 'template-parts/featured-image' ); ?>
                    

                    <?php if (is_page('service-repair-center')) { ?>
                        <div class="row">
                            <?php $service = new WP_Query( array( 'post_type' => 'service', 'posts_per_page' => -1 ) );
                            while( $service->have_posts() ) : $service->the_post(); ?>
                                <div class="medium-4 columns service-col">
                                    <?php the_post_thumbnail(); ?>
                                    <h4><?php the_title(); ?></h4>
                                    <?php the_content(); ?>
                                </div>
                            <?php endwhile; wp_reset_query(); ?>
                        </div>
                    
                    <?php } elseif (is_page('testimonials')) { ?>
                        <div class="row">
                            <?php $testimonial = new WP_Query( array( 'post_type' => 'testimonial', 'posts_per_page' => -1 ) );
                            while( $testimonial->have_posts() ) : $testimonial->the_post(); ?>
                                <div class="medium-4 columns service-col">
                                    <?php the_post_thumbnail(); ?>
                                    <h4><?php the_title(); ?></h4>
                                    <?php the_content(); ?>
                                </div>
                            <?php endwhile; wp_reset_query(); ?>
                        </div>

                    <?php } elseif (is_page('contact-us')) { ?>
                        <div class="row contact-page">
                            <div class="large-6 columns contact-content">
                                <?php the_content(); ?>
                            </div>
                            <div class="large-6 columns contact-form">
                                <h3>Get in Touch With Us</h3>
                                <p>Please fill out all fields below for questions/comments.</p>
                                <?php echo do_shortcode( '[contact-form-7 id="20" title="Contact form 1"]' ); ?>
                            </div>
                        </div>

                    <?php } else { ?>
                        <?php the_content(); ?>
                    <?php } ?>
                </div>
            </div>
        </div>

    <?php endwhile;?>

</div>

<?php get_footer();