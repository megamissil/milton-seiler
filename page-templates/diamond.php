<?php

/*
Template Name: Diamond Background
*/

get_header('diamond'); ?>

<div id="page" role="main">
    <header class="row subpage-title">
        <div class="small-12 columns">
            <h1><?php the_title(); ?></h1>

            <hr>
        </div>
    </header>

    <div class="row">
        <div class="small-12 columns">
            <?php the_content(); ?>
        </div>
    </div>
</div>

<?php get_footer(); 