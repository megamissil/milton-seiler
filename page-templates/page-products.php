<?php
/*
Template Name: Product Listing
*/

get_header();

// Retrieve data associated with this page
$page = get_post();

// Retrieve all posts that belong to this page's category (related by name)
$taxonomy = array(
    'taxonomy' => 'category',
    'field'    => 'slug',
    'terms'    => $page->post_name
);

$posts = get_posts(array(
    'post_type'      => 'product',
    'post_status'    => 'publish',
    'orderby'        => 'title',
    'order'          => 'ASC',
    'posts_per_page' => -1,
    'tax_query'      => array($taxonomy)
));

// The total number of rows to output
$total = ceil( count($posts) / 3 ); 

// Chunk posts into groups of three
$posts = array_chunk($posts, 3); ?>

<div id="page" role="main">
    <header class="row subpage-title">
        <div class="small-12 columns">
            <h1><?= $page->post_title ?></h1> <hr>          
        </div>
    </header>

    <div class="row">
        <div class="small-12 columns" data-equalizer data-equalize-on="large">
            <div class="subpage-content subpage-content-nobg">
                <aside class="small-12 large-3 columns product-category-sidebar" data-equalizer-watch>
                    <ul>
                        <li>
                            <h4><a href="/products/air-handling-equipment/">Air Handling Equipment</a></h4>
                        </li>

                        <li>
                            <h4><a href="/products/air-handling-equipment/">Gas Handling Equipment</a></h4>
                        </li>

                        <li>
                            <h4><a href="/products/pumps-accessories/">Pumps And Accessories</a></h4>
                        </li>

                        <li>
                            <h4><a href="/products/process-equipment/">Process Equipment</a></h4>
                        </li>

                        <li>
                            <h4><a href="/products/solids-handling/">Solids Handling</a></h4>
                        </li>

                    </ul>
                </aside>

                <div class="small-12 large-9 columns" data-equalizer-watch>
                    <?php if ( empty( $posts ) ): ?>
                        <p>There are no products for this category.</p>
                    <?php else: ?>
                        <?php for ( $i = 0; $i < $total; $i++ ): ?>
                            <ul class="row product-cards">
                                <?php foreach ( $posts[$i] as $post ): get_post($post); ?>
                                    <a data-open="productModal<?= get_the_ID() ?>">
                                        <li class="product-card">
                                            <?= get_the_post_thumbnail() ?>
                                        </li>
                                    </a>
                                <?php endforeach; ?>
                            </ul>
                        <?php endfor; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ( !empty($posts) ): ?>
    <?php for ( $i = 0; $i < $total; $i++ ): ?>
        <?php foreach ( $posts[$i] as $post ): get_post($post); ?>
            <div class="reveal vendor-modal" id="productModal<?= get_the_ID() ?>" data-reveal>
                <div class="row">
                    <div class="small-6 columns">
                        <div class="product-card within-modal">
                            <?= get_the_post_thumbnail() ?>
                        </div>
                    </div>

                    <div class="small-6 columns vendor-meta">
                        <h1><a href="<?= get_post_meta( get_the_ID(), 'external-url', true ) ?>"><?= get_the_title() ?></a></h1>

                        <p class="lead"><?= $post->post_content ?></p>
                    </div>
                </div>

                <div class="row">
                    <div class="small-12 columns text-center">
                        <h3><a class="vendor-link" href="<?= get_post_meta( get_the_ID(), 'external-url', true ) ?>">Click here to visit this vendor's website.</a></h3>
                    </div>
                </div>

                <button class="close-button" data-close aria-label="Close window" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endforeach; ?>
    <?php endfor; ?>
<?php endif; ?>

<?php get_footer(); ?>