<?php
/**
 * Template Name: News
 *
 * The template for displaying company news posts.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

query_posts(array(
    'category_name'  => 'company-news',
    'post_status'    => 'publish',
    'posts_per_page' => -1
));

get_header(); ?>

<div id="page" role="main">
    <div class="row subpage-title">
        <div class="small-12 columns">
            <h1><?php the_title(); ?></h1> 
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="small-12 medium-11 large-10 medium-centered columns">
            <div class="subpage-content">
                <?php while (have_posts()) : the_post(); ?>
                    <div class="row">
                        <div class="small-12 columns">
                            <?php get_template_part( 'template-parts/content', get_post_format() ); ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer();