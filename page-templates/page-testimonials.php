<?php
/**
 * Template Name: Testimonials
 *
 * A template for displaying testimonial post types. Relies on the 'diamond' header.
 *
 * Updated to use a separate background image than the "Default Page"
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

$page = get_post();

$wp_query = new WP_Query(array(
    'post_type'      => 'testimonial',
    'posts_per_page' => -1,
    'order'          => 'ASC'
));

get_header('diamond'); ?>

<div id="page" role="main">
    <header class="row subpage-title">
        <div class="small-12 columns">
            <h1><?= $page->post_title ?></h1> <hr>          
        </div>
    </header>

    <div class="row">
        <div class="small-12 columns">
            <div class="subpage-content">
                <?php if ( $wp_query->have_posts() ) : ?>
                    <?php foreach ( $wp_query->posts as $post ): get_post($post); ?>
                        <article class="testimonial">
                            <header>
                                <h5 class="red"><?= types_render_field( "testimonial-location", array() ) ?></h5> // <h5><?=types_render_field( "testimonial-reference", array() ) ?></h5>
                            </header>

                            <p><em><?= $post->post_content ?></em></p>
                        </article>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer();