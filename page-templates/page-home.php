<?php
/*
Template Name: Home
*/
get_header(); ?>

<!-- HOME BUTTONS & NEWS -->
<section class="home-buttons-news">
	<div class="row">
		<div class="large-8 columns">
			<div class="row home-buttons">
				<div class="medium-6 columns">
					<a href="/service-repair-center" class="button col-height">Service &amp; Repair Center</a>
				</div>
				<div class="medium-6 columns">
					<a href="/testimonials" class="button col-height">Testimonials</a>
				</div>
				<div class="medium-6 columns">
					<a href="/products/air-handling-equipment" class="button col-height">Food Industry</a>
				</div>
				<div class="medium-6 columns">
					<a href="/featured-product" class="button col-height">Featured Products</a>
				</div>
				<div class="medium-6 columns">
					<a href="/contact-us" class="button col-height">Request a Quote</a>
				</div>
				<div class="medium-6 columns">
					<a href="/about-us" class="button col-height">Our Story</a>
				</div>
			</div>
		</div>
		<div class="large-4 columns">
			<div class="home-news">
				<h3>Milton Seiler Company News</h3>
				<hr>

				<?php
					$args = array(
						'post_type' 	 => 'post',
						'posts_per_page' => 2,
						'tax_query' => array(array(
							'taxonomy' 	 => 'category',
							'field' 	 => 'slug',
							'terms' 	 => 'company-news'
						))
					);
					$articles = new WP_Query( $args );
					if ( $articles->have_posts() ) {
						while ( $articles->have_posts() ) {
							$articles->the_post();
							?>
								<div class="article">
									<h4><?php the_title(); ?></h4>
									<p class="date"><?php the_time('M j, Y'); ?></p>
									<a href="<?php the_permalink(); ?>" class="button">Read More</a>
								</div>
							<?php 
						}
					} else {
						echo '<p>Posts coming soon!</p>';
					}
			  	?>
				
			</div><!-- /.home-news -->
		</div>
	</div>
</section>
<!-- /.home-buttons-news -->

<!-- CONNECT WITH US -->
<section class="connect-with-us">
	<div class="row">
		<div class="medium-10 medium-centered columns">
			<div class="columns text-center">
				<h2>Connect with Us!</h2>
				<hr>
			</div>

			<div class="medium-6 columns">
				<div class="fb-page" data-href="https://www.facebook.com/1voigtab" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"></div></div>
			</div>
			<div class="medium-6 columns text-center newsletter" id="newsletter">
				<div class="row collapse">
					<div class="small-12 columns">
						<div class="form-container">
							<p>Sign up for our E-Newsletter!</p>

							<?= do_shortcode('[mc4wp_form id="45"]') ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.connect-with-us -->

<?php get_footer();
