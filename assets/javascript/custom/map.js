var MarkerJS = (function (archive_url, map_id) {
    return {
        archive: archive_url,
        map_id:  map_id,

        createMap: function (zoom, center) {
            return new google.maps.Map(
                document.getElementById(map_id),
                { zoom: zoom, center: center }
            );
        },

        parseContent: function (data, callback) {
            var output = 
                '<div id="content">' +
                '<div id="siteNotice">' + '</div>' +
                '<h3 id="firstHeading" class="firstHeading">%title%</h3>' +
                '<div id="bodyContent">' +
                '<p>%address% <br> %city% %phone% %fax%' +
                '</div>' + '</div>';

            output = output.replace('%title%', data.title);
            output = output.replace('%address%', data.address.line);
            
            if (data.address.line !== '') {
                output = output.replace('%city%', data.address.city);
            } else {
                output = output.replace('%city%', '')
            }

            if (data.phone !== null) {
                output = output.replace('%phone%', '<br> <strong>Phone:</strong> ' + data.phone);
            } else {
                output = output.replace('%phone%', '');
            }

            if (data.fax !== null) {
                output = output.replace('%fax%', '<br> <strong>Fax:</strong> ' + data.fax);
            } else {
                output = output.replace('%fax%', '');
            }

            callback(output);
        },

        addMarker: function (map, location, slug, content) {
            var info = new google.maps.InfoWindow({
                content: content
            });

            var marker = new google.maps.Marker({
                position: location,
                map: map
            });

            marker.addListener('click', function () {
                info.open(map, marker);
            });
        },

        geocodeAddress: (function geocodeAddress(address, callback) {
            var geocoder = new google.maps.Geocoder(),
                result   = false;
            
            geocoder.geocode({address: address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var location = results[0].geometry.location;
                    
                    result = {
                        lat: location.lat(),
                        lng: location.lng()
                    };
                } else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                    // Credit for this block goes to SO user Bryan Weaver: http://stackoverflow.com/questions/9805529/geocoding-api-over-query-limit
                    setTimeout(function() {
                        geocodeAddress(address, callback);
                    }, 200);

                    return;
                } else {                    
                    if (console) {
                        console.warn('Geocoding of ' + address + ' failed: ' + status);
                    }
                }
                
                callback(result);
            });
        })
    };
});

function initMap() {
    "use strict";

    // Replace these values with those that relate to your project.
    var Map = MarkerJS('/map-archive', 'voigtAbMap');

    var element = Map.createMap(6, {lat: 33.5792415, lng: -86.6441016}),
        markers = jQuery.get( Map.archive );

    markers.success(function (marker_list) {
        for (var marker in marker_list) {
            marker = marker_list[marker];

            Map.parseContent(marker, function (content) {
                var full_address = marker.address.line + ', ' + marker.address.city;

                Map.geocodeAddress(full_address, function (location) {
                    if (location === false) {
                        return;
                    } else {
                        Map.addMarker(element, location, marker.slug, content);
                    }
                });
            });
        }
    });

    markers.fail(function (jqXHR, status, error) {
        if (console) {
            console.warn(error);
        }
    });
}
