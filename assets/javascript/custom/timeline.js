$(document).ready(function () {
    var timeline = $('.timeline-screen');

    $('.timeline-screen-open').click(function () {
        timeline.slideDown(300);

        $('body').addClass('noscroll');
    });

    $('.timeline-screen-close').click(function () {
        timeline.slideUp(300);

        $('body').removeClass('noscroll');
    });
});
