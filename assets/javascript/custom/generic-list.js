(function ($) {
    $('.toggle-generic-list-item').click(function() {
        if ($(this).hasClass('active')) {            
            return;
        }

        var active = $('.toggle-generic-list-item.active');
        
        var active_panel_id = active.attr('data-generic-list-panel'),
            target_panel_id = $(this).attr('data-generic-list-panel');

        active.removeClass('active');
        $(this).addClass('active');

        $('[data-generic-list-id="' + active_panel_id + '"]').removeClass('visible');
        $('[data-generic-list-id="' + target_panel_id + '"]').addClass('visible');
    });
})(jQuery);