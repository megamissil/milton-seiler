
$(document).ready(function(){
  $('.home-slider').slick({
	autoplay: true,
	fade: true,
	prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left"></i></button>',
  	nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right"></i></button>'
  });
});