<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

		</section>
		<div id="footer-container">
			<div class="row footer-info">
				<div class="small-12 large-6 columns">
					<div class="row">
						<div class="small-6 columns">
							<a href="/submit-your-resume" class="button">Upload Your Resume</a>
						</div>
						<div class="small-6 columns">
							<a href="/request-a-quote" class="button">Request a Quote</a>
						</div>
					</div>
				</div>
				<div class="small-12 medium-6 large-2 columns">
					<div class="social-icons">
						<a href="" target="_blank"><i class="fa fa-facebook-square"></i></a>
						<a href="https://twitter.com/voigtabernathy"><i class="fa fa-twitter-square"></i></a>
						<a href="https://www.linkedin.com/company/voigt-abernathy-company-inc-"><i class="fa fa-linkedin-square"></i></a>
					</div>
				</div>
			</div>

			<footer id="footer">
				<div class="row">
					<div class="small-12 medium-8 large-6 columns copy">
						Copyright &copy; <?=date('Y'); ?> Milton Seiler. All Rights Reserved.
					</div>
					<div class="small-12 medium-4 large-6 columns moxy">
						<a href="http://digmoxy.com">Moxy</a>
					</div>
				</div>
			</footer>
		</div>

		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
		</div><!-- Close off-canvas wrapper inner -->
	</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>

<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
<?php if ( is_page('locations') ): ?>
	<script src="<?= get_stylesheet_directory_uri() ?>/assets/javascript/custom/map.js"></script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsSW6lXoRCQqTJN70nrRQZyFwa06SY3oY&callback=initMap"></script>
<?php endif; ?>
</body>
</html>