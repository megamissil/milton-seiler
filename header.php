<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!-- Favicon -->
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<?php wp_head(); ?>

		<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Fjalla+One|Source+Sans+Pro:400,400italic,600,600italic' rel='stylesheet' type='text/css'>
		<!-- END -->

		<!-- Owl Carousel Styles -->
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/components/owl-carousel-2/dist/assets/owl.carousel.css" />
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/components/owl-carousel-2/dist/assets/owl.theme.default.css" />
	</head>
	<body <?php body_class(); ?>>

	<script type="text/javascript">
		//<![CDATA[
		var DID=84086;
		var pssl=(window.location.protocol == "https:") ? "https://stats.sa-as.com/lib.js":"http://stats.sa-as.com/lib.js";
		document.writeln('<scr'+'ipt async src="'+pssl+'" type="text\/javascript"><\/scr'+'ipt>');
		//]]>
	</script>
		

	<?php include('analytics.php');?>

	<!-- Facebook page plugin -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	
	<?php do_action( 'foundationpress_after_body' ); ?>

	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
		<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
	<?php endif; ?>

	<?php do_action( 'foundationpress_layout_start' ); ?>

	<header id="masthead" class="site-header <?php if (!is_home()) {echo 'add-border';} ?>" role="banner">
		<!-- LOGO & TAGLINE -->
		<div class="row header-logo">
			<div class="small-10 medium-8 large-6 small-centered columns">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ms-logo-full-reg.png" alt="">
				</a>
			</div>
		</div>
		<!-- END -->

		<div class="title-bar" data-responsive-toggle="site-navigation">
			<button class="menu-icon" type="button" data-toggle="mobile-menu"></button>
		</div>

		<nav id="site-navigation" class="main-navigation top-bar <?php if (!is_home()) {echo 'remove-border';} ?>" role="navigation">
			<div class="top-bar-left">
				<?php /*<ul class="menu">
					<li class="home"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></li>
				</ul>*/ ?>
			</div>
			<div class="top-bar-right">
				<?php foundationpress_top_bar_r(); ?>

				<?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) == 'topbar' ) : ?>
					<?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
				<?php endif; ?>
			</div>
		</nav>

		<?php if (is_front_page()) { ?>
			<!-- HOME SLIDER -->
			<div class="row collapse">
				<div class="medium-10 medium-centered columns">
					<div class="home-slider owl-theme">
			          	<?php
					    $args = array(
					      	'post_type' => 'home-slide',
					    );
					    $products = new WP_Query( $args );
					    if( $products->have_posts() ) {
					      	while( $products->have_posts() ) {
					        	$products->the_post();
					        	?>
					        		<div>
					        			<?php the_post_thumbnail(); ?>

					        			<?php if ( get_the_content() ): ?>
						        			<div class="caption show-for-medium">
						        				<?php the_content(); ?>
						        			</div>
					        			<?php endif; ?>
					        		</div>
					        	<?php
					      	}
					    }
					  	?>
				  	</div><!-- /.home-slider -->
				</div>
			</div>
			<!-- END -->

			<!-- CTA BUTTONS -->
			<div class="cta-buttons">
				<div class="row buttons">
					<div class="columns text-center">
						<a href="/request-a-quote" class="button">Request A Quote</a>
						<a href="" class="button">Newsletter Signup</a>
						<a href="/air-handling-equipment" class="button">Air Handling Equipment</a>
					</div>
				</div>
			</div>
			<!-- END -->

		<?php } ?>
	</header>

	<section class="container">
		<?php do_action( 'foundationpress_after_header' );
